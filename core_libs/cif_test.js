!function() {

  var cif = {};
	function loadBaseBlueprints(bp) {
		sfdb.clearEverything();
		return loadSocialStructure( bp );
	};
	cif.loadBaseBlueprints=loadBaseBlueprints;

	
	
	var loadQueue=[];var loadTimeout;var loadXhr;
	function loadFile()//variadic function using arguments 
	{
		if (!window.XMLHttpRequest) {
			console.log("Browser doesn't support XMLHttpRequest.");
			return false;
		}
		for(var i=0;i<arguments.length;i++)
		{
			loadQueue.push(arguments[i]);
		}
		if (typeof loadTimeout == "undefined"){loadTimeout=setTimeout(loadAsync,0);}
	}
	cif.loadFile=loadFile;
	var loadAsync=function()
	{//loads queued files one by one, to ensure loading order is the same as the order in which files are given; the queue may also contain callbacks
		if(loadQueue.length==0)
		{
			loadTimeout=undefined;
			return;
		}
		var nextItem=loadQueue.shift();
		if(typeof nextItem=="string")
		{
			//assuming a file path
			loadXhr = new XMLHttpRequest();
			loadXhr.open("GET", file, true);
			loadXhr.onreadystatechange = function() {
				if ((this.status == 200)&&(this.readyState==4)) 
				{
					addData(JSON.parse(loadXhr.responseText));
					loadTimeout=setTimeout(loadAsync,0);
				}
			};
			loadXhr.send();
		}
		if(typeof nextItem=="function")
		{
			setTimeout(function(){nextItem();loadTimeout=setTimeout(loadAsync,0);},0);
		}
		
	}

	
	function addData(data)
	{
		//replaces original separate loading functions for the schema, volition/trigger rules, actions, cast, history...
		if("schema" in data){loadSocialStructure(data);}
		if("cast" in data){addCharacters(data);}//TODO: randomized characters?
		if("triggerRules" in data){addRules(data);}
		if("volitionRules" in data){addRules(data);}
		if("actions" in data){addActions(data);}
		if("history" in data){addHistory(data);}
	}
	cif.addData=addData;//so users can add data without loading a file
	
	
	
	function registerSocialType (blueprint) {
		var factory = {};
		factory.class = blueprint.class;
		factory.type = blueprint.type;
		factory.directionType = blueprint.directionType;
		factory.isBoolean = blueprint.isBoolean;
		factory.cloneEachTimeStep = blueprint.cloneEachTimeStep;
		factory.duration = blueprint.duration;
		factory.min = blueprint.minValue;
		factory.max = blueprint.maxValue;
		factory.defaultVal = blueprint.defaultValue;
		factory.allowIntent = blueprint.allowIntent;
		return factory;
	}

	var socialStructure;
cif.getSocialStructure=function getSocialStructure()
{return socialStructure;}

	cif.loadSocialStructure = function loadSocialStructure(data) {
		var structure = {};

		var blueprints;
		try {
			if (typeof data === "string") {
				blueprints = (JSON.parse(data)).schema;
			} else if (typeof data === "object") {
				blueprints = data.schema;
			} else {
				console.log("unexpected value:", data);
				throw new Error("Error load social structure: unexpected data value: ", typeof data);
			}
		} catch (e) {
			throw new Error("JSON Error load social structure (blueprints): " + e);
		}
		if (blueprints === undefined) {
			throw new Error("Error: social structure data file must be JSON that defines a top-level key 'schema'");
		}
		var atLeastOneClassAllowsIntent = false;
		for (var i = 0; i < blueprints.length; i++) {
			var classBlueprint = blueprints[i];

			// Error Checking
			if (classBlueprint.allowIntent === true) {
				atLeastOneClassAllowsIntent = true;
			}
			if (structure[classBlueprint.class]) {
				throw new Error("DATA ERROR in CiF.loadSocialStructure: the class '" + classBlueprint.class + "' is defined more than once.");
			}

			validate.blueprint(classBlueprint, "Examining blueprint  #" + i);

			sfdb.registerDuration(classBlueprint);
			sfdb.registerDefault(classBlueprint);
			sfdb.registerDirection(classBlueprint);
			sfdb.registerIsBoolean(classBlueprint);
			sfdb.registerMaxValue(classBlueprint);
			sfdb.registerMinValue(classBlueprint);

			// Create an interface for each type within this class.
			structure[classBlueprint.class] = {};
			for (var j = 0; j < classBlueprint.types.length; j++) {
				var type = classBlueprint.types[j].toLowerCase();
				var typeBlueprint = util.clone(classBlueprint);
				typeBlueprint.type = type;
				structure[classBlueprint.class][type] = registerSocialType(typeBlueprint);
			}

		}

		if (!atLeastOneClassAllowsIntent) {
			throw new Error("SCHEMA ERROR: A schema must include at least one class where allowIntent is true, otherwise there are no possible actions for characters to take.");
		}

		socialStructure = structure;
		validate.registerSocialStructure(socialStructure);
		return socialStructure;
	};


	cif.getClassDescriptors = function getClassDescriptors(className) {
		var descriptors = {};
		var c = socialStructure[className];
		if (c === undefined) {
			return false;
		}
		// The details for every type within a class should be the same, so just go with the first one.
		for (var typeName in c) {
			var t = c[typeName];
			descriptors.directionType = t.directionType;
			descriptors.isBoolean = t.isBoolean;
			descriptors.cloneEachTimeStep = t.cloneEachTimeStep === undefined ? true : t.cloneEachTimeStep;
			descriptors.duration = t.duration;
			descriptors.min = t.min;
			descriptors.max = t.max;
			descriptors.defaultVal = t.defaultVal;
			return descriptors;
		}
		// If the class was somehow empty, also return false.
		return false;
	}


	cif.getClassFromType=function getClassFromType(type) {
		for (var className in socialStructure) {
			if (socialStructure[className][type] !== undefined) {
				return className;
			}
		}
		return false;
	}

	cif.isValidTypeForClass = function isValidTypeForClass(type, className) {
		var cn = socialStructure[className];
		if (cn === undefined) return false;
		if (cn[type] === undefined) return false;
		return true;
	}


	var getSortedTurnsTuple = function(tab) {
		var t0Val = tab[0];
		var t1Val = tab[1];
		if (t0Val === "START") {
			t0Val = 9999999999;
		}
		if (t0Val === "NOW") {
			t0Val = 0;
		}
		if (t1Val === "START") {
			t1Val = 9999999999;
		}
		if (t1Val === "NOW") {
			t1Val = 0;
		}
		if (t0Val > t1Val) {
			var tmp = tab[0];
			tab[0] = tab[1];
			tab[1] = tmp;
		}
		return tab;
	}

	var savedChars;


	cif.addCharacters = function addCharacters(data) {
		// STUB: For the moment we aren't doing anything with this data,
		// other than returning an array of keys.
		var charData = data;
		var chars = charData.cast;
		savedChars = chars;
		return getCharacters();
	};


	var getCharacters = function() {
		return _.keys(savedChars);
	};
	var getCharactersWithMetadata = function() {
		return util.clone(savedChars);
	};
	var getCharData = function(char, key) {
		if (savedChars[char] === undefined) {
			return undefined;
		}
		return savedChars[char][key];
	};
	var getCharName = function(char) {
		var name = getCharData(char, "name");

		// If name is undefined, just return the character's ID.
		return name || char;
	};

	var addProcessedRules = function(ruleType, fileName, rules) {

		var conditionValFunc;
		var effectValFunc;

		var ids = [];

		if (ruleType === "trigger" || ruleType === "volition") {
			conditionValFunc = ruleType === "trigger" ? validate.triggerCondition : validate.volitionCondition;
			effectValFunc = ruleType === "trigger" ? validate.triggerEffect : validate.volitionEffect;
			ruleType = ruleType + "Rules";
		}

		// Validate data.
		var i, j, rule, tab;
		for (i = 0; i < rules.length; i++) {

			rule = rules[i];
			if (rule.name === undefined) {
				console.log("Warning: " + ruleType + " Rule #" + i + " is missing a 'name'.");
			}
			//Store the 'origin' of this rule, so we'll always be able to know where it came from, and give it a unique ID.
			rule.origin = fileName;
			var newId = ruleType + "_" + util.iterator("rules");
			ids.push(newId);
			rule.id = newId;

			// Check conditions
			if (conditionValFunc !== undefined) {
				for (j = 0; j < rule.conditions.length; j++) {
					conditionValFunc(rule.conditions[j], "Examining " + ruleType + " rule #" + i + ": '" + rule.name + "' Validating condition at position " + j);
					// Sort turnsAgoBetween tuple, if present, low-hi.
					if (rule.conditions[j].turnsAgoBetween !== undefined) {
						rule.conditions[j].turnsAgoBetween = getSortedTurnsTuple(rule.conditions[j].turnsAgoBetween);
					}
				}
			}

			// Check effects
			if (effectValFunc !== undefined) {
				for (j = 0; j < rule.effects.length; j++) {
					effectValFunc(rule.effects[j], "Examining " + ruleType + " rule #" + i + ": '" + rule.name + "' Validating effect at position " + j);
				}
			}
		}

		if (rules.length > 0) {
			ruleLibrary.addRuleSet(ruleType, rules);
			return ids;
		} else {
			return [];
		}
	}

	var addRules = function(data){
		var parsedData;
		var ruleType;
		var fileName;
		var rules;

		try {
			if (typeof data === "string") {
				parsedData = JSON.parse(data);
			} else if (typeof data === "object") {
				parsedData = data;
			} else {
				console.log("unexpected value:", data);
				throw new Error("Error adding rules: unexpected data value: ", typeof data);
			}
		} catch (e) {
			throw new Error("JSON Error loading rules: " + e);
		}
		console.log("parsedData", parsedData);

		fileName = parsedData.fileName;
		rules = parsedData.rules;
		ruleType = parsedData.type;

		if (rules === undefined) {
			throw new Error("Error: " + ruleType + " rules data file must include a top-level key 'rules'");
		}
		if (ruleType === undefined) {
			throw new Error("Error: " + ruleType + " rules data file must include a top-level key 'type' that is either 'trigger', 'volition', or a custom ruleset key.");
		}

		return addProcessedRules(ruleType, fileName, rules);

	};

	var getRules = function(ruleSet) {
		if (ruleSet === "trigger") {
			return ruleLibrary.getTriggerRules();
		}
		if (ruleSet === "volition") {
			return ruleLibrary.getVolitionRules();
		}
		console.log("No functionality yet for retrieving a ruleset that's neither trigger nor volition.");
		return [];
	}

	var setRuleById = function(label, rule) {

		var ruleSet = label.split("_")[0];

		// Ensure the new rule is valid.
		var results = validate.rule(rule);
		if (typeof results === "string") {
			// Validation failed.
			console.log("Tried to setRulesById for '" + label + "' but validation failed: " + results);
			return false;
		}

		// DISABLED check for existing rule, b/c this flags as true when updating a rule! What we actually need is a way for isRuleAlreadyInRuleSet to return the ID of the matching rule(s), so we could allow that in this case.
		// if (ruleLibrary.isRuleAlreadyInRuleSet(ruleSet, rule)) {
		// 	console.log("Tried to setRulesById for '" + label + "' but an identical rule already exists.");
		// 	return false;
		// }

		return ruleLibrary.setRuleById(label, rule);
	}

	// Public-facing function to access the SFDB. Does verification on input. Internal functions should use sfdb.get instead.
	var getSFDB = function(searchPredicate, mostRecentTime, lessRecentTime) {

		// TODO: Make sure operator is not + or -

		// Ensure time window. Set to 0 if undefined.
		mostRecentTime = mostRecentTime || 0;
		lessRecentTime = lessRecentTime || 0;

		// Convert turnsAgoBetween to time window.
		if (searchPredicate.turnsAgoBetween !== undefined) {
			mostRecentTime += searchPredicate.turnsAgoBetween[0];
			lessRecentTime += searchPredicate.turnsAgoBetween[1];
		}

		// Ensure proper time window ordering.
		if (mostRecentTime > lessRecentTime){
			var tmp = mostRecentTime;
			mostRecentTime = lessRecentTime;
			lessRecentTime = tmp;
		}

		// Ensure SFDB has been initialized.
		if (sfdb.getCurrentTimeStep() === -1) {
			sfdb.setupNextTimeStep(0);
		}

		return sfdb.get(searchPredicate, mostRecentTime, lessRecentTime);
	};

	//public facing function to put a character offstage.

	var setCharacterOffstage = function(characterName){
		sfdb.putCharacterOffstage(characterName);
	};

	//public facing function to see if a character is offstage or not.
	var getIsCharacterOffstage = function(characterName){
		return(sfdb.getIsCharacterOffstage(characterName));
	};

	//public facing function to place a character onstage.
	var setCharacterOnstage = function(characterName){
		sfdb.putCharacterOnstage(characterName);
	};

	//public facing function to see if a character is onstage or not.
	var getIsCharacterOnstage	= function(characterName){
		var characterOffstage = sfdb.getIsCharacterOffstage(characterName);
		return (!characterOffstage);
	};

	//public facing function to see if a character has been eliminated.
	var setCharacterEliminated = function(characterName){
		sfdb.eliminateCharacter(characterName);
	};

	//public facing function to see if a character has been eliminated or not.
	var getIsCharacterEliminated  = function(characterName){
		sfdb.getIsCharacterEliminated(characterName);
	};

	//public facing function to make two characters perform an action.
	var doAction = function(actionName, initiator, responder, registeredVolitions){
		actionLibrary.doAction(actionName, initiator, responder, registeredVolitions);
	};

	var reset = function() {
		// Clear all social structure info.
		socialStructure = undefined;

		// Clear all character info
		// For now, we aren't storing this anyway.

		// Clear the SFDB History.
		sfdb.clearEverything();

		// Clear all rules.
		ruleLibrary.clearRuleLibrary();
	};


	cif.init = function init() {
		sfdb.init();
		return "Ok";
	};
//TODO: move the interface definition to each function, and put all other cif code together
		getCharacters			: getCharacters,
		getCharactersWithMetadata : getCharactersWithMetadata,
		getCharData				: getCharData,
		getCharName				: getCharName,

		loadBaseBlueprints		: loadBaseBlueprints,
		loadFile				: loadFile,

		calculateVolition		: ruleLibrary.calculateVolition,
		runTriggerRules			: ruleLibrary.runTriggerRules,
		ruleToEnglish			: ruleLibrary.ruleToEnglish,
		predicateToEnglish		: ruleLibrary.predicateToEnglish,

		dumpSFDB				: sfdb.dumpSFDB,
		set						: sfdb.set,
		get						: getSFDB,
		setCharacterOffstage	: setCharacterOffstage,
		getIsCharacterOffstage	: getIsCharacterOffstage,
		setCharacterOnstage		: setCharacterOnstage,
		getIsCharacterOnstage	: getIsCharacterOnstage,
		setCharacterEliminated	: setCharacterEliminated,
		getIsCharacterEliminated : getIsCharacterEliminated,
		setupNextTimeStep		: sfdb.setupNextTimeStep,
		getRegisteredDirection	: sfdb.getRegisteredDirection,
		getAction				: actionLibrary.getAction,
		getActions				: actionLibrary.getActions,
		addActions				: actionLibrary.parseActions,
		clearActionLibrary		: actionLibrary.clearActionLibrary,//needed to update rules on the fly
		addHistory				: sfdb.addHistory,

		addRules				: addRules,
		getRules				: getRules,
		setRuleById				: setRuleById,
		getRuleById				: ruleLibrary.getRuleById,
		deleteRuleById			: ruleLibrary.deleteRuleById,

		doAction				: doAction,

		reset					: reset,
		getSFDBForD3			: sfdb.getSFDBCopyAtTimestep,
		getCloneFullSFDB		: sfdb.cloneSFDB,
		setSFDB					: sfdb.setSFDB,
		getCurrentTimeStep		: sfdb.getCurrentTimeStep
	};


	cif = cifInterface;

	var event = document.createEvent('Event');
	event.initEvent('cifLoaded', true, true);
	document.dispatchEvent(event);

	return cifInterface;
}